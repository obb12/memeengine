# Endpoints


## graphql




> Example request:

```bash
curl -X GET \
    -G "http://localhost/graphql" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/graphql"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "errors": [
        {
            "message": "GraphQL Request must include at least one of those two parameters: \"query\" or \"queryId\"",
            "extensions": {
                "category": "request"
            },
            "trace": [
                {
                    "file": "\/var\/www\/html\/vendor\/nuwave\/lighthouse\/src\/GraphQL.php",
                    "line": 103,
                    "call": "GraphQL\\Server\\Helper::validateOperationParams()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/nuwave\/lighthouse\/src\/Support\/Utils.php",
                    "line": 97,
                    "call": "Nuwave\\Lighthouse\\GraphQL::Nuwave\\Lighthouse\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/nuwave\/lighthouse\/src\/GraphQL.php",
                    "line": 102,
                    "call": "Nuwave\\Lighthouse\\Support\\Utils::applyEach()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/nuwave\/lighthouse\/src\/Support\/Http\/Controllers\/GraphQLController.php",
                    "line": 28,
                    "call": "Nuwave\\Lighthouse\\GraphQL::executeRequest()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
                    "line": 48,
                    "call": "Nuwave\\Lighthouse\\Support\\Http\\Controllers\\GraphQLController::__invoke()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
                    "line": 254,
                    "call": "Illuminate\\Routing\\ControllerDispatcher::dispatch()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
                    "line": 197,
                    "call": "Illuminate\\Routing\\Route::runController()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
                    "line": 693,
                    "call": "Illuminate\\Routing\\Route::run()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 128,
                    "call": "Illuminate\\Routing\\Router::Illuminate\\Routing\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/nuwave\/lighthouse\/src\/Support\/Http\/Middleware\/AttemptAuthentication.php",
                    "line": 32,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Nuwave\\Lighthouse\\Support\\Http\\Middleware\\AttemptAuthentication::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/nuwave\/lighthouse\/src\/Support\/Http\/Middleware\/AcceptJson.php",
                    "line": 27,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Nuwave\\Lighthouse\\Support\\Http\\Middleware\\AcceptJson::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 103,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
                    "line": 695,
                    "call": "Illuminate\\Pipeline\\Pipeline::then()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
                    "line": 670,
                    "call": "Illuminate\\Routing\\Router::runRouteWithinStack()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
                    "line": 636,
                    "call": "Illuminate\\Routing\\Router::runRoute()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
                    "line": 625,
                    "call": "Illuminate\\Routing\\Router::dispatchToRoute()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
                    "line": 166,
                    "call": "Illuminate\\Routing\\Router::dispatch()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 128,
                    "call": "Illuminate\\Foundation\\Http\\Kernel::Illuminate\\Foundation\\Http\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
                    "line": 21,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
                    "line": 21,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
                    "line": 27,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
                    "line": 86,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
                    "line": 37,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Fruitcake\\Cors\\HandleCors::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
                    "line": 57,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 167,
                    "call": "Fideloper\\Proxy\\TrustProxies::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
                    "line": 103,
                    "call": "Illuminate\\Pipeline\\Pipeline::Illuminate\\Pipeline\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
                    "line": 141,
                    "call": "Illuminate\\Pipeline\\Pipeline::then()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
                    "line": 110,
                    "call": "Illuminate\\Foundation\\Http\\Kernel::sendRequestThroughRouter()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
                    "line": 324,
                    "call": "Illuminate\\Foundation\\Http\\Kernel::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
                    "line": 305,
                    "call": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls::callLaravelOrLumenRoute()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
                    "line": 76,
                    "call": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls::makeApiCall()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
                    "line": 51,
                    "call": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls::makeResponseCall()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
                    "line": 41,
                    "call": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls::makeResponseCallIfEnabledAndNoSuccessResponses()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
                    "line": 236,
                    "call": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls::__invoke()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
                    "line": 172,
                    "call": "Knuckles\\Scribe\\Extracting\\Generator::iterateThroughStrategies()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
                    "line": 127,
                    "call": "Knuckles\\Scribe\\Extracting\\Generator::fetchResponses()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
                    "line": 119,
                    "call": "Knuckles\\Scribe\\Extracting\\Generator::processRoute()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
                    "line": 73,
                    "call": "Knuckles\\Scribe\\Commands\\GenerateDocumentation::processRoutes()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
                    "line": 36,
                    "call": "Knuckles\\Scribe\\Commands\\GenerateDocumentation::handle()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
                    "line": 40,
                    "call": "Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
                    "line": 93,
                    "call": "Illuminate\\Container\\Util::unwrapIfClosure()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
                    "line": 37,
                    "call": "Illuminate\\Container\\BoundMethod::callBoundMethod()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
                    "line": 610,
                    "call": "Illuminate\\Container\\BoundMethod::call()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
                    "line": 136,
                    "call": "Illuminate\\Container\\Container::call()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/symfony\/console\/Command\/Command.php",
                    "line": 256,
                    "call": "Illuminate\\Console\\Command::execute()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
                    "line": 121,
                    "call": "Symfony\\Component\\Console\\Command\\Command::run()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
                    "line": 971,
                    "call": "Illuminate\\Console\\Command::run()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
                    "line": 290,
                    "call": "Symfony\\Component\\Console\\Application::doRunCommand()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
                    "line": 166,
                    "call": "Symfony\\Component\\Console\\Application::doRun()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
                    "line": 93,
                    "call": "Symfony\\Component\\Console\\Application::run()"
                },
                {
                    "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
                    "line": 129,
                    "call": "Illuminate\\Console\\Application::run()"
                },
                {
                    "file": "\/var\/www\/html\/artisan",
                    "line": 37,
                    "call": "Illuminate\\Foundation\\Console\\Kernel::handle()"
                }
            ]
        }
    ]
}
```
<div id="execution-results-GETgraphql" hidden>
    <blockquote>Received response<span id="execution-response-status-GETgraphql"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETgraphql"></code></pre>
</div>
<div id="execution-error-GETgraphql" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETgraphql"></code></pre>
</div>
<form id="form-GETgraphql" data-method="GET" data-path="graphql" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETgraphql', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETgraphql" onclick="tryItOut('GETgraphql');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETgraphql" onclick="cancelTryOut('GETgraphql');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETgraphql" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>graphql</code></b>
</p>
<p>
<small class="badge badge-black">POST</small>
 <b><code>graphql</code></b>
</p>
</form>


## api/user




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```
<div id="execution-results-GETapi-user" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user"></code></pre>
</div>
<div id="execution-error-GETapi-user" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user"></code></pre>
</div>
<form id="form-GETapi-user" data-method="GET" data-path="api/user" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user" onclick="tryItOut('GETapi-user');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user" onclick="cancelTryOut('GETapi-user');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user</code></b>
</p>
</form>


## /




> Example request:

```bash
curl -X GET \
    -G "http://localhost/" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/cerulean/bootstrap.min.css" integrity="sha384-3fdgwJw17Bi87e1QQ4fsLn4rUFqWw//KU0g8TvV6quvahISRewev6/EocKNuJmEw" crossorigin="anonymous">
 <link rel="stylesheet" href="/css/app.css">
    <title>Memes</title>
  </head>
  <body>
    <div id="root"></div>
    <script src="js/index.js"></script>
  </body>
</html>

```
<div id="execution-results-GET-" hidden>
    <blockquote>Received response<span id="execution-response-status-GET-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GET-"></code></pre>
</div>
<div id="execution-error-GET-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GET-"></code></pre>
</div>
<form id="form-GET-" data-method="GET" data-path="/" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GET-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GET-" onclick="tryItOut('GET-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GET-" onclick="cancelTryOut('GET-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GET-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>/</code></b>
</p>
</form>


## memes




> Example request:

```bash
curl -X GET \
    -G "http://localhost/memes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/memes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 1,
        "name": "Hello Adele",
        "link": "https:\/\/i.imgflip.com\/um3ro.jpg",
        "created_at": "2021-02-18T06:16:47.000000Z",
        "updated_at": "2021-02-18T09:07:43.000000Z"
    },
    {
        "id": 2,
        "name": "Thanos",
        "link": "https:\/\/brobible.com\/wp-content\/uploads\/2020\/10\/50-best-memes-falcons-braves.jpg",
        "created_at": "2021-02-18T07:10:16.000000Z",
        "updated_at": "2021-02-18T07:10:16.000000Z"
    },
    {
        "id": 3,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:28:36.000000Z",
        "updated_at": "2021-02-18T07:28:36.000000Z"
    },
    {
        "id": 4,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:28:39.000000Z",
        "updated_at": "2021-02-18T07:28:39.000000Z"
    },
    {
        "id": 5,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:28:54.000000Z",
        "updated_at": "2021-02-18T07:28:54.000000Z"
    },
    {
        "id": 6,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:31:42.000000Z",
        "updated_at": "2021-02-18T07:31:42.000000Z"
    },
    {
        "id": 7,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:34:18.000000Z",
        "updated_at": "2021-02-18T07:34:18.000000Z"
    },
    {
        "id": 8,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:38:23.000000Z",
        "updated_at": "2021-02-18T07:38:23.000000Z"
    },
    {
        "id": 9,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:39:12.000000Z",
        "updated_at": "2021-02-18T07:39:12.000000Z"
    },
    {
        "id": 10,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:40:22.000000Z",
        "updated_at": "2021-02-18T07:40:22.000000Z"
    },
    {
        "id": 11,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:41:17.000000Z",
        "updated_at": "2021-02-18T07:41:17.000000Z"
    },
    {
        "id": 12,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:42:12.000000Z",
        "updated_at": "2021-02-18T07:42:12.000000Z"
    },
    {
        "id": 13,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:42:56.000000Z",
        "updated_at": "2021-02-18T07:42:56.000000Z"
    },
    {
        "id": 14,
        "name": "Kyogre",
        "link": "https:\/\/static.fandomspot.com\/images\/05\/6870\/001-pokemon-catching-kyogre-meme.jpg",
        "created_at": "2021-03-04T15:58:42.000000Z",
        "updated_at": "2021-03-04T15:58:42.000000Z"
    },
    {
        "id": 15,
        "name": "Bud light",
        "link": "https:\/\/i.pinimg.com\/originals\/43\/37\/74\/4337742d3b5dc87a8bfe7b4b254a49c6.jpg",
        "created_at": "2021-03-04T16:00:06.000000Z",
        "updated_at": "2021-03-04T16:00:06.000000Z"
    },
    {
        "id": 16,
        "name": "Annoying Girlfriend",
        "link": "https:\/\/img.memecdn.com\/who-is-she_o_423439.jpg",
        "created_at": "2021-03-04T16:01:56.000000Z",
        "updated_at": "2021-03-04T16:01:56.000000Z"
    },
    {
        "id": 17,
        "name": "school",
        "link": "https:\/\/destinationksa.com\/wp-content\/uploads\/2017\/04\/feature-image-710x434.jpg",
        "created_at": "2021-03-04T16:03:15.000000Z",
        "updated_at": "2021-03-04T16:03:15.000000Z"
    },
    {
        "id": 18,
        "name": "school",
        "link": "https:\/\/destinationksa.com\/wp-content\/uploads\/2017\/04\/1.jpg",
        "created_at": "2021-03-04T16:03:51.000000Z",
        "updated_at": "2021-03-04T16:03:51.000000Z"
    },
    {
        "id": 19,
        "name": "programming",
        "link": "https:\/\/miro.medium.com\/max\/1920\/0*z1mm6izqSeDiKukb",
        "created_at": "2021-03-04T16:04:51.000000Z",
        "updated_at": "2021-03-04T16:04:51.000000Z"
    },
    {
        "id": 20,
        "name": "programming",
        "link": "https:\/\/i.redd.it\/9u4tx0d53es11.png",
        "created_at": "2021-03-04T16:05:16.000000Z",
        "updated_at": "2021-03-04T16:05:16.000000Z"
    }
]
```
<div id="execution-results-GETmemes" hidden>
    <blockquote>Received response<span id="execution-response-status-GETmemes"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETmemes"></code></pre>
</div>
<div id="execution-error-GETmemes" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETmemes"></code></pre>
</div>
<form id="form-GETmemes" data-method="GET" data-path="memes" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETmemes', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETmemes" onclick="tryItOut('GETmemes');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETmemes" onclick="cancelTryOut('GETmemes');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETmemes" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>memes</code></b>
</p>
</form>


## memes/create/{ext}




> Example request:

```bash
curl -X POST \
    "http://localhost/memes/create/quia" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/memes/create/quia"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTmemes-create--ext-" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTmemes-create--ext-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTmemes-create--ext-"></code></pre>
</div>
<div id="execution-error-POSTmemes-create--ext-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTmemes-create--ext-"></code></pre>
</div>
<form id="form-POSTmemes-create--ext-" data-method="POST" data-path="memes/create/{ext}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTmemes-create--ext-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTmemes-create--ext-" onclick="tryItOut('POSTmemes-create--ext-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTmemes-create--ext-" onclick="cancelTryOut('POSTmemes-create--ext-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTmemes-create--ext-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>memes/create/{ext}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>ext</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="ext" data-endpoint="POSTmemes-create--ext-" data-component="url" required  hidden>
<br>
</p>
</form>


## memes/{id}




> Example request:

```bash
curl -X PUT \
    "http://localhost/memes/aperiam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/memes/aperiam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


<div id="execution-results-PUTmemes--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTmemes--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTmemes--id-"></code></pre>
</div>
<div id="execution-error-PUTmemes--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTmemes--id-"></code></pre>
</div>
<form id="form-PUTmemes--id-" data-method="PUT" data-path="memes/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTmemes--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTmemes--id-" onclick="tryItOut('PUTmemes--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTmemes--id-" onclick="cancelTryOut('PUTmemes--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTmemes--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>memes/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="PUTmemes--id-" data-component="url" required  hidden>
<br>
</p>
</form>


## memes/{id}




> Example request:

```bash
curl -X PATCH \
    "http://localhost/memes/nostrum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/memes/nostrum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PATCH",
    headers,
}).then(response => response.json());
```


<div id="execution-results-PATCHmemes--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-PATCHmemes--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PATCHmemes--id-"></code></pre>
</div>
<div id="execution-error-PATCHmemes--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PATCHmemes--id-"></code></pre>
</div>
<form id="form-PATCHmemes--id-" data-method="PATCH" data-path="memes/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PATCHmemes--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PATCHmemes--id-" onclick="tryItOut('PATCHmemes--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PATCHmemes--id-" onclick="cancelTryOut('PATCHmemes--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PATCHmemes--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>memes/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="PATCHmemes--id-" data-component="url" required  hidden>
<br>
</p>
</form>


## memes/search.{ext}




> Example request:

```bash
curl -X GET \
    -G "http://localhost/memes/search.eum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/memes/search.eum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 1,
        "name": "Hello Adele",
        "link": "https:\/\/i.imgflip.com\/um3ro.jpg",
        "created_at": "2021-02-18T06:16:47.000000Z",
        "updated_at": "2021-02-18T09:07:43.000000Z"
    },
    {
        "id": 2,
        "name": "Thanos",
        "link": "https:\/\/brobible.com\/wp-content\/uploads\/2020\/10\/50-best-memes-falcons-braves.jpg",
        "created_at": "2021-02-18T07:10:16.000000Z",
        "updated_at": "2021-02-18T07:10:16.000000Z"
    },
    {
        "id": 3,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:28:36.000000Z",
        "updated_at": "2021-02-18T07:28:36.000000Z"
    },
    {
        "id": 4,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:28:39.000000Z",
        "updated_at": "2021-02-18T07:28:39.000000Z"
    },
    {
        "id": 5,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:28:54.000000Z",
        "updated_at": "2021-02-18T07:28:54.000000Z"
    },
    {
        "id": 6,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:31:42.000000Z",
        "updated_at": "2021-02-18T07:31:42.000000Z"
    },
    {
        "id": 7,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:34:18.000000Z",
        "updated_at": "2021-02-18T07:34:18.000000Z"
    },
    {
        "id": 8,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:38:23.000000Z",
        "updated_at": "2021-02-18T07:38:23.000000Z"
    },
    {
        "id": 9,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:39:12.000000Z",
        "updated_at": "2021-02-18T07:39:12.000000Z"
    },
    {
        "id": 10,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:40:22.000000Z",
        "updated_at": "2021-02-18T07:40:22.000000Z"
    },
    {
        "id": 11,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:41:17.000000Z",
        "updated_at": "2021-02-18T07:41:17.000000Z"
    },
    {
        "id": 12,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:42:12.000000Z",
        "updated_at": "2021-02-18T07:42:12.000000Z"
    },
    {
        "id": 13,
        "name": "Sally",
        "link": "http:\/google.com",
        "created_at": "2021-02-18T07:42:56.000000Z",
        "updated_at": "2021-02-18T07:42:56.000000Z"
    },
    {
        "id": 14,
        "name": "Kyogre",
        "link": "https:\/\/static.fandomspot.com\/images\/05\/6870\/001-pokemon-catching-kyogre-meme.jpg",
        "created_at": "2021-03-04T15:58:42.000000Z",
        "updated_at": "2021-03-04T15:58:42.000000Z"
    },
    {
        "id": 15,
        "name": "Bud light",
        "link": "https:\/\/i.pinimg.com\/originals\/43\/37\/74\/4337742d3b5dc87a8bfe7b4b254a49c6.jpg",
        "created_at": "2021-03-04T16:00:06.000000Z",
        "updated_at": "2021-03-04T16:00:06.000000Z"
    },
    {
        "id": 16,
        "name": "Annoying Girlfriend",
        "link": "https:\/\/img.memecdn.com\/who-is-she_o_423439.jpg",
        "created_at": "2021-03-04T16:01:56.000000Z",
        "updated_at": "2021-03-04T16:01:56.000000Z"
    },
    {
        "id": 17,
        "name": "school",
        "link": "https:\/\/destinationksa.com\/wp-content\/uploads\/2017\/04\/feature-image-710x434.jpg",
        "created_at": "2021-03-04T16:03:15.000000Z",
        "updated_at": "2021-03-04T16:03:15.000000Z"
    },
    {
        "id": 18,
        "name": "school",
        "link": "https:\/\/destinationksa.com\/wp-content\/uploads\/2017\/04\/1.jpg",
        "created_at": "2021-03-04T16:03:51.000000Z",
        "updated_at": "2021-03-04T16:03:51.000000Z"
    },
    {
        "id": 19,
        "name": "programming",
        "link": "https:\/\/miro.medium.com\/max\/1920\/0*z1mm6izqSeDiKukb",
        "created_at": "2021-03-04T16:04:51.000000Z",
        "updated_at": "2021-03-04T16:04:51.000000Z"
    },
    {
        "id": 20,
        "name": "programming",
        "link": "https:\/\/i.redd.it\/9u4tx0d53es11.png",
        "created_at": "2021-03-04T16:05:16.000000Z",
        "updated_at": "2021-03-04T16:05:16.000000Z"
    }
]
```
<div id="execution-results-GETmemes-search.-ext-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETmemes-search.-ext-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETmemes-search.-ext-"></code></pre>
</div>
<div id="execution-error-GETmemes-search.-ext-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETmemes-search.-ext-"></code></pre>
</div>
<form id="form-GETmemes-search.-ext-" data-method="GET" data-path="memes/search.{ext}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETmemes-search.-ext-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETmemes-search.-ext-" onclick="tryItOut('GETmemes-search.-ext-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETmemes-search.-ext-" onclick="cancelTryOut('GETmemes-search.-ext-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETmemes-search.-ext-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>memes/search.{ext}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>ext</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="ext" data-endpoint="GETmemes-search.-ext-" data-component="url" required  hidden>
<br>
</p>
</form>


## memes/{id}




> Example request:

```bash
curl -X DELETE \
    "http://localhost/memes/quae" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/memes/quae"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEmemes--id-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEmemes--id-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEmemes--id-"></code></pre>
</div>
<div id="execution-error-DELETEmemes--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEmemes--id-"></code></pre>
</div>
<form id="form-DELETEmemes--id-" data-method="DELETE" data-path="memes/{id}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEmemes--id-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEmemes--id-" onclick="tryItOut('DELETEmemes--id-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEmemes--id-" onclick="cancelTryOut('DELETEmemes--id-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEmemes--id-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>memes/{id}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="id" data-endpoint="DELETEmemes--id-" data-component="url" required  hidden>
<br>
</p>
</form>



