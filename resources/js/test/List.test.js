// Link.react.test.js
import React from 'react';
import { render } from '@testing-library/react';
import { shallow, mount,configure } from "enzyme";
import ReactDOM from 'react-dom';
import TestUtils from 'react-dom/test-utils';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import List from '../List';

configure({ adapter: new Adapter() });
test('List renders', () => {

  shallow(<List memes={[{"id":1,"name":"Hello Adele","link":"https:\/\/i.imgflip.com\/um3ro.jpg","created_at":"2021-02-18T06:16:47.000000Z","updated_at":"2021-02-18T09:07:43.000000Z"},{"id":2,"name":"Thanos","link":"https:\/\/brobible.com\/wp-content\/uploads\/2020\/10\/50-best-memes-falcons-braves.jpg","created_at":"2021-02-18T07:10:16.000000Z","updated_at":"2021-02-18T07:10:16.000000Z"}]}/>);

});
