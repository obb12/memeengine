// Link.react.test.js
import React from 'react';
import { render } from '@testing-library/react';
import { shallow, mount,configure } from "enzyme";
import ReactDOM from 'react-dom';
import TestUtils from 'react-dom/test-utils';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import CreateForm from '../CreateForm';

configure({ adapter: new Adapter() });
test('CreateForm renders', () => {

  shallow(<CreateForm />);

});
it('gets the form state from onSubmit function', () => {
  const wrapper = document.createElement('div');
  const onSubmitFn = jest.fn(data => data);
  ReactDOM.render(
    <CreateForm sendcreate={onSubmitFn}/>,
    wrapper
  );
  const form = wrapper.querySelector('form');
  TestUtils.Simulate.submit(form);
  expect(onSubmitFn).toHaveBeenCalledTimes(1);
});
