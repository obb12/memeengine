
  import React, { useState } from "react"

  function Search(props) {
    return (
      <div className="container">
      <div className="row justify-content-md-center">
        <input ref={props.textInput} type="text" placeholder="Search" />
        <div className="btn-group" role="group" aria-label="Basic example">
        <button onClick={props.sendMessage} className="btn btn-primary">Search</button>
        <button onClick={props.togglecreate} className="btn btn-info">Create</button>
        </div>
        </div>
        </div>

  );
    }
    export default Search;
