import React, { useState } from "react"

function List(props) {
  return (
    <ul className="list-group">
    {props.memes.map(function (meme) {
      return(
  <li className="list-group-item" key={meme.id} ><img src={meme.link}/></li>);
    })}
    </ul>

);
  }
  export default List;
