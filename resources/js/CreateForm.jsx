import React, { useState } from "react"

function CreateForm(props) {
  return (
<div className="container">
<form onSubmit={props.sendcreate}>
<div className={props.showcreate ? "row justify-content-md-center display" : "row justify-content-md-center dontdisplay"}>
<input ref={props.nameinput} placeholder="meme name"/>
<input ref={props.linkinput} placeholder="meme link"/>
<button onClick={props.sendcreate} className="btn btn-info">Save</button>
</div>

</form>
    </div>);
  }
  export default CreateForm;
