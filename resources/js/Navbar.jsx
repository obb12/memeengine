import React from "react"

function Navbar() {
    return (
<nav className="navbar navbar-expand-lg navbar-light">
<div class="container">
<div class="row">
<div class="col-md-4 offset-md-8"><a className="navbar-brand" href="#">Meminator</a></div>
</div>
</div>
</nav>
    );
}
export default Navbar;
