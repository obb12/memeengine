import React, { useState } from "react"
import axios from "axios";
import Navbar from "./Navbar";
import CreateForm from "./CreateForm";
import List from "./List";
import Search from "./Search"
function App() {
      const [memes, setMemes] = useState([]);
      const [showcreate, setShowcreate] = useState(false);
      const togglecreate = (e) => {
        setShowcreate(!showcreate);
}
const sendcreate = (e) => {
  e.preventDefault();
  axios.post('/memes/create/json', {
    name: nameinput.current.value,
    link: linkinput.current.value
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}
const nameinput = React.createRef();
const linkinput = React.createRef();

      const textInput = React.createRef();
  const sendMessage = (e) => {

    e.preventDefault();

    axios.get('/memes/search.json', {
    params: {
      term: textInput.current.value
    }
  })
  .then(function (response) {
    console.log(response);
    setMemes(response.data)
  })
  .catch(function (error) {
    console.log(error);
  })
  .then(function () {
    // always executed
  });


  }
  return (
    <div className="App  ">
  <Navbar/>
  <div className="search">


      <CreateForm nameinput={nameinput} linkinput={linkinput} showcreate={showcreate} sendcreate={sendcreate} />

    <Search sendMessage={sendMessage} togglecreate={togglecreate} textInput={textInput} />
   </div>
<List memes={memes} />
    </div>
  );
}

export default App;
