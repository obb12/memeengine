<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use MarvinRabe\LaravelGraphQLTest\TestGraphQL;

class SearchTest extends TestCase
{
      use RefreshDatabase;
      use TestGraphQL;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create()
    {
      $response = $this->postJson('/memes/create/json', ['name' => 'Sally','link' => 'http:/google.com']);

     $response
         ->assertJson([
             'created' => true,
         ]);
    }
    public function test_createwithoutname()
    {
      $response = $this->postJson('/memes/create/json', ['link' => 'http:/google.com']);

     $response
         ->assertJson([
             'message' => "The given data was invalid.",

             'errors' => [
               'name' => [
                 'The name field is required.'
               ]
             ]
         ]);
    }

    public function test_graphql(){
      $response = $this->postJson('/memes/create/json', ['name' => 'Sally','link' => 'http:/google.com']);

      $this->query('memes', ['name' => '%Sally%'], ['name'])
      ->assertSuccessful()
      ->assertJsonFragment([
        'name' => 'Sally'
      ]);
    }
    public function test_createwithoutlink()
    {
      $response = $this->postJson('/memes/create/json', ['name' => 'http:/google.com']);

     $response
         ->assertJson([
             'message' => "The given data was invalid.",

             'errors' => [
               'link' => [
                 'The link field is required.'
               ]
             ]
         ]);
    }
    public function test_creeateempty()
    {
      $response = $this->postJson('/memes/create/json', );

     $response
         ->assertJson([
             'message' => "The given data was invalid.",

             'errors' => [
               'link' => [
                 'The link field is required.'
               ],'name' => [
                 'The name field is required.'
               ]
             ]
         ]);
    }
    public function test_search()
    {
      $response = $this->get('/memes/search.json');

     $response
         ->assertSuccessful();
    }
}
