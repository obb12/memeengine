<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meme;
class MemeController extends Controller
{
    //
    public function index()
    {
      return Meme::all();
    }
    public function create(Request $request,$ext)
    {
      $validated = $request->validate([
        'name' => 'required|max:255',
        'link' => 'required',
    ]);
      $meme = new Meme;
      $meme->name =  $request->name;
      $meme->link = $request->link;
      $meme->save();
      $response = [
        'created' => true,
      ];
      return $ext== "xml" ? response()->xml($response) : response()->json($response);
    }
    public function delete(Request $request,$id){
      $meme = Meme::find($id);
      $meme->delete();
    }
    public function update(Request $request,$id){
      $meme = Meme::find($id);
      $meme->name =  $request->name;
      $meme->link = $request->link;
      $meme->save();
    }
    public function patch(Request $request,$id){
      $meme = Meme::find($id);
      if($request->has("link")){
        $meme->link = $request->link;
      }
      if($request->has("name")){
        $meme->name  = $request->name;
      }
      $meme->save();
    }
    public function search(Request $request,$ext){

      $data = Meme::whereRaw('UPPER(name) LIKE ?', ['%' . strtoupper($request->term) . '%'])->get();
      return $ext== "xml" ? response()->xml($data) : response()->json($data);
    }
}
