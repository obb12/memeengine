<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/memes', [MemeController::class, 'index']);
Route::post('/memes/create/{ext}', [MemeController::class, 'create']);
Route::put('/memes/{id}', [MemeController::class, 'update']);
Route::patch('/memes/{id}', [MemeController::class, 'patch']);

Route::get('/memes/search.{ext}', [MemeController::class, 'search']);
Route::delete('/memes/{id}', [MemeController::class, 'delete']);

